<h1 align="center">MicroPython QRCode Research</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

### 二维码介绍

Overview

![](./images/QR-Code-Overview.png)


test
![](./images/qrcode_test.png)

### 参考文档

* [QR Code Tutorial](https://www.thonky.com/qr-code-tutorial/)
* [qr_standard.pdf](https://www.swisseduc.ch/informatik/theoretische_informatik/qr_codes/docs/qr_standard.pdf)
* [二维码的生成细节和原理](https://blog.csdn.net/hk_5788/article/details/50839790)
* [QRCode.com | DENSO WAVE](https://www.qrcode.com/zh/)
* [Reed-Solomon 编码（翻译）](http://article.iotxfd.cn/RFID/Reed%20Solomon%20Codes)
* [Generator Polynomial Tool](https://www.thonky.com/qr-code-tutorial/generator-polynomial-tool?degree=17)
* [GB/T 18284-2000标准下载](http://www.biaozhun8.cn/so.asp?k=GB%2FT+18284-2000)

### 合作交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
	* 走线物联：[163271910](https://jq.qq.com/?_wv=1027&k=xtPoHgwL)
	* 扇贝物联：[31324057](https://jq.qq.com/?_wv=1027&k=yp4FrpWh)

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
